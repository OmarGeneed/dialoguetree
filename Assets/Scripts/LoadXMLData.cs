using UnityEngine;
using System.Collections;
using System.Xml;
using System.Text;
using System.Reflection;
using System.IO;
using System.Collections.Generic;

public class LoadXMLData : MonoBehaviour
{
	public List<DialogueNode> conversations;
	private string spriteName;

	// Use this for initialization
	void Start ()
	{
		conversations = new List<DialogueNode>();
		TextAsset dialogueTree = Resources.Load ("dialogue") as TextAsset;
		string xmlString = dialogueTree.text;
		
	// Create an XmlReader
		using (XmlReader reader = XmlReader.Create (new StringReader (xmlString))) {
			
			int nodeParent=0;
			int nodeNumber=0;
			DialogueNode dn = null;
						
			while (reader.Read()) {
				if (reader.IsStartElement ()) {
					switch (reader.Name) {
					case "npc":
						reader.MoveToFirstAttribute ();
						nodeParent = int.Parse(reader.Value);
						reader.MoveToAttribute("image");
						spriteName = reader.Value; 
						break;
					case "question":
						if(dn!=null) conversations.Add(dn);
						dn = new DialogueNode();
						reader.MoveToFirstAttribute ();
						nodeNumber = int.Parse(reader.Value);
						dn.setNodeParent(nodeParent);
						dn.setSprite(spriteName);
						dn.setNodeNumber(nodeNumber);
						dn.setQuestion(reader.ReadString());
						break;
					case "response":
						reader.MoveToFirstAttribute ();
						nodeNumber = int.Parse(reader.Value);
						dn.addResponseAndLink(reader.ReadString(), nodeNumber);
						break;					
					}
				}
			}
			if(dn!=null) conversations.Add(dn);
		}
		
	}
}
