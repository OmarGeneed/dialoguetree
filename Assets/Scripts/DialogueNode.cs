using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DialogueNode {

	private int nodeParent = 0;
	private int nodeNumber = 0;
	private string question = "";
	private List<string> responses = new List<string>();
	private List<int> responsesLinks = new List<int>();
	private Sprite sprite;
	
	
	public void setNodeParent(int _nodeParent) {
		nodeParent=_nodeParent;	
	}
	
	public int getNodeParent() {
		return nodeParent;
	}
	
	public void setNodeNumber(int _nodeNumber) {
		nodeNumber=_nodeNumber;	
	}
	
	public int getNodeNumber(){
		return nodeNumber;
	}
	
	public void setQuestion(string _question) {
		question=_question;
	}
	
	public string getQuestion(){
		return question;
	}
	
	public void setResponses(List<string> _responses) {
		responses = _responses;
	}
	
	public List<string> getResponses() {
		return responses;
	}
	
	public void setResponsesLinks(List<int> _responsesLinks) {
		responsesLinks = _responsesLinks;
	}
	
	public List<int> getResponsesLinks() {
		return responsesLinks;
	}
	
	public void addResponseAndLink(string _response, int _link) {
		responses.Add(_response);
		responsesLinks.Add(_link);
	}

	public void setSprite(string _sprite)
	{
		sprite = Resources.Load<Sprite>(_sprite);
	}

	public Sprite getSprite()
	{
		return sprite;
	}

}
